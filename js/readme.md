# JavaScript

## Custom Event Trigger (non-DOM)

    document.dispatchEvent(new CustomEvent("SomeEvent", {
            msg: "AnythingHere",
            bubbles: true,
            cancelable: true
        })
    );

    document.addEventListener("SomeEvent", function() {
        alert('after an event');
    }, false);
