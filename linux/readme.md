# Linux

## Get Video Card

    $ sudo update update-pciids
    $ lspci -v
    $ lspci -v | grep VGA
    
## Vagrant VBGuest & Fix Bug:

    $ vagrant plugin install vagrant-vbguest

    $ vagrant ssh
    $ sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions


## VirtualBox w/Windows 8+:

    $ vboxmanage list vms
    $ vboxmanage setextradata "The Box Name" VBoxInternal/CPUM/CMPXCHG16B 1
